var zetta = require('zetta'),
    uuid = require('node-uuid'),
    auth = require('zetta-peer-auth'),
    path = require('path'),
    fs = require('fs'),
    MemoryDeviceRegistry = require('./lib/memory_device_registry'),
    MemoryPeerRegistry = require('./lib/memory_peer_registry'),
    DeviceRegistry = require('./lib/device-registry'),
    PeerRegistry = require('./lib/peer-registry');

module.exports = JiliaSandbox;

/**
 * Constructor function accepts:
 * - zero arguments: All defaults used. No devices or apps loaded.
 * - one argument:
 *   + Object of all options (optionally including `devices` and/or `apps`) or
 *   + Array of devices
 * - two arguments: `devices` array and options object (optionally including `apps`)
 *
 * `devices` is expected to be an array of arrays in the format:
 * ```
 * [
 *  [Constructor, param1, para2, ..],
 *  [Constructor2, param1],
 * ]
 * ```
 *
 * This allows you to pass in a list of "fake" devices to be included in the sandbox.
 *
 * To have persistent server and device ids, you can pass provide "persistenceId" in
 * the options.  The persistence data will be stored in "{path}/.data/<persistenceId>"
 * where {path} is either the `cwd()` or `opts.dataRoot`.
 *
 * It the `link` options property is provided, the sandbox will attempt to peer using the
 * following values:
 * ```
 * link: {
 *   url: '{custom} | https://api.jilia.io/v1',
 *   oauth: '{custom} | {url}/oauth/accesstoken',
 *   credentials: '{key}:{secret}' // Valid hub app credentials
 * }
 * ```
 */
function JiliaSandbox(devices, opts) {
  if(arguments.length === 1) {
    // Only arg is not array, must be `opts` object
    if(!Array.isArray(devices)) {
      opts = devices;
      devices = opts.devices || [];
    }
  }

  opts = opts || {}

  this.server;

  /**
   * Server's address. Is of the form:
   *
   * `{ port: 12346, family: 'IPv4', address: '127.0.0.1' }`
   *
   * {@link https://nodejs.org/dist/latest-v4.x/docs/api/net.html#net_server_address node doc}
   */
  this.address;

  /**
   * Path to persisted sandbox data.
   */
  this.dataDirectory;

  // either use the provided name or create one
  var name = opts.name || 'sandbox-' + uuid.v4();

  var zettaOpts = {}
  if(opts.persistenceId) {
    var root = opts.dataRoot || process.cwd();
    this.dataDirectory = path.join(root, '.data', opts.persistenceId);
    try {
      mkdir(this.dataDirectory);
    } catch(err) {
      console.log(err);
    }

    // persistence enabled, load name from directory
    var idPath = path.join(this.dataDirectory, 'id');
    try {
      name = fs.readFileSync(idPath, 'ascii');
      console.log("Loaded name: ", name);
    } catch(err) {
      fs.writeFileSync(idPath, name, 'ascii');
    }

    zettaOpts.registry = new DeviceRegistry(this.dataDirectory);
    zettaOpts.peerRegistry = new PeerRegistry(this.dataDirectory);
  } else {
    // persistence not enabled, use memory only
    zettaOpts.registry = new MemoryDeviceRegistry();
    zettaOpts.peerRegistry = new MemoryPeerRegistry();
  }

  var server = zetta(zettaOpts);
  server.name(name);

  if(opts.link) {
    var url = opts.link.url || 'https://api.jilia.io/v1';

    var useAuth = Object.keys(opts.link).indexOf('oauth') !== -1;
    if(useAuth) {

      var oauth = opts.link.oauth || url + '/oauth/accesstoken';
      server.use(auth({
        headers: {
          'Authorization': new Buffer(opts.link.credentials).toString('base64'),
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        url: oauth,
        method: 'POST',
        body: 'grant_type=client_credentials'
      }));

    }

    server.link(url);
  }

  if(devices) {
    devices.forEach(function(dev) {
      console.log('Creating ' + dev[1]);
      server.use.apply(server, dev);
    });
  }

  var apps = opts.apps;
  if(apps) {
    console.log('Loading %s app%s', apps.length, apps.length == 1 ? '' : 's');
    apps.forEach(function(app){
      server.use(app);
    });
  }

  this.server = server;
}

/**
 * Start the server. Possible arguments match those
 * of the node net `Server` module's `listen` method.
 *
 * https://nodejs.org/docs/latest-v0.12.x/api/net.html#net_class_net_server
 */
JiliaSandbox.prototype.listen = function() {
  this.server.listen.apply(this.server, arguments);
};

/**
 * Close the server socket.
 * @param {function} cb
 */
JiliaSandbox.prototype.close = function(cb) {
  this.server.httpServer.server.close(cb);
};

/**
 * The server address object is only available after a succesful
 * call to `listen`; otherwise the result will be `null`.
 */
JiliaSandbox.prototype.getServerAddress = function() {
  return this.server.httpServer.server.address();
};


/**
 * Recursively create the provided directory path.
 * @param {string} p - Directory path.
 */
function mkdir(p) {
  p = path.resolve(p);

  try {
    fs.mkdirSync(p);
  } catch(err) {
    if(err.code === 'ENOENT') {
      mkdir(path.dirname(p));
      mkdir(p);
    } else {
      var stat;
      try {
        stat = fs.statSync(p);
      } catch(err1) {
        throw err;
      }
      if(!stat.isDirectory()) {
        throw err;
      }
    }
  }
}
