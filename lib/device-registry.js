var zetta = require('zetta'),
    util = require('util'),
    path = require('path');

function DeviceRegistry(p) {
  var loc = path.join(p,'.devices');
  zetta.DeviceRegistry.call(this, { path: loc, collection: 'devices' });
};

util.inherits(DeviceRegistry, zetta.DeviceRegistry);

module.exports = DeviceRegistry;