var zetta = require('zetta'),
    util = require('util'),
    path = require('path');

function PeerRegistry(p) {
  var loc = path.join(p,'.peers');
  zetta.PeerRegistry.call(this, { path: loc, collection: 'peers' });
};

util.inherits(PeerRegistry, zetta.PeerRegistry);

module.exports = PeerRegistry;